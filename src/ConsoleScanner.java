import java.util.Scanner;

public class ConsoleScanner {
	
	Scanner scanner;
	private boolean b;
	@SuppressWarnings("unused")
	private Thread thread;
	
	public ConsoleScanner(){
		
		System.out.println("[CLIENT] ConsoleScanner wird geladen ...");
		
		this.scanner = new Scanner(System.in);
		this.b = true;
		
		System.out.println("[CLIENT] ConsoleScanner wurde geladen");
	}
	
	public void start(){
		System.out.println("[CLIENT] ConsoleScanner wurde gestartet");
		
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				while(b){
					String data = scanner.nextLine();
					String[] cmd = data.split(" ");
					if(cmd != null){
						executeCommand(cmd);
					}
				}
			}
		});
		thread.start();
	}
	
	public void stop(){
		scanner.close();
		this.b = false;
		
		System.out.println("[CLIENT] ConsoleScanner wurde gestoppt");
	}
	
	
	
	
	private void executeCommand(String[] cmd){
		switch(cmd[0].toLowerCase()){
		case "system":
			
			if(cmd.length < 2){
				return;
			}
			
			switch(cmd[1].toLowerCase()){
			case "reload":
				main.c.reload();
				break;
			case "stop":
				main.c.shutdown();
				break;
			}
			
			break;
		}
	}
	
	

}

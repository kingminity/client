import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Settings{
	
	int clientID;
	int RAM;
	String csIP;
	int csPort;
	
	private String path;
	
	public Settings(){
		
		System.out.println("[CLIENT] Settings werden ausgelesen ...");
		
		this.clientID = 12345;
		this.RAM = 10000;
		this.csIP = "0";
		this.csPort = 0;
		
		File filepath = new File("");
		this.path = filepath.getAbsolutePath();
		
		File f = new File(this.path, "Settings.properties");
		if(! f.isFile()){
			System.out.println("[CLIENT] Settings.properties wurde nicht gefunden");
			
			Properties serverdata = new Properties();
			serverdata.setProperty("ClientID", "0");
			serverdata.setProperty("RAM", "0");
			serverdata.setProperty("CloudSystem_IP", "0");
			serverdata.setProperty("CloudSystem_Port", "0");
			try {
				FileOutputStream out = new FileOutputStream(f);
				serverdata.store(out, null);
				out.close();
				
				System.out.println("[CLIENT] Neue Settings.properties wurde erstellt");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		Properties p = new Properties();
		FileInputStream in;
		try {
			in = new FileInputStream(f);
			p.load(in);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		this.clientID = Integer.parseInt(p.getProperty("ClientID"));
		this.RAM = Integer.parseInt(p.getProperty("RAM"));
		this.csIP = p.getProperty("CloudSystem_IP");
		this.csPort = Integer.parseInt(p.getProperty("CloudSystem_Port"));
		
		
		if(this.clientID == 0 || this.RAM == 0 || this.csIP == "0" || this.csPort == 0){
			System.out.println("[CLIENT] Settings sind ungültig");
			
			main.c.shutdown();
		} else {
			System.out.println("[CLIENT] Settings wurden erfolgreich ausgelesen");
		}
	}
	
	public void reload(){
		System.out.println("[CLIENT] Settings werden neu geladen ...");
		System.out.println("[CLIENT] Die Client ID wird dabei nicht berücksichtigt");
		
		File f = new File(this.path, "Settings.properties");
		Properties p = new Properties();
		FileInputStream in;
		try {
			in = new FileInputStream(f);
			p.load(in);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		this.RAM = Integer.parseInt(p.getProperty("RAM"));
		this.csIP = p.getProperty("CloudSystem_IP");
		this.csPort = Integer.parseInt(p.getProperty("CloudSystem_Port"));
		
		
		if(this.RAM == 0 || this.csIP == "0" || this.csPort == 0){
			System.out.println("[CLIENT] Settings sind ungültig");
			
			main.c.shutdown();
		} else {
			System.out.println("[CLIENT] Settings wurden erfolgreich neu geladen");
		}
	}
	
	
	
	

}

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

public class Client implements CloudListener {

	CloudConnector cc;
	Settings settings;
	ConsoleScanner scanner;

	public Client() {

	}

	public void start() {
		this.settings = new Settings();
		this.scanner = new ConsoleScanner();
		this.scanner.start();
		this.cc = new CloudConnector(settings.csIP, settings.csPort, this);
	}

	public void reload() {
		System.out.println("[CLIENT] System wird neu geladen ...");
		this.settings.reload();

		this.cc.write("CLIENT RELOAD " + this.settings.clientID + " " + this.settings.RAM);
		System.out.println("[CLIENT] System wurde erfolgreich neu geladen");
	}

	public void shutdown() {
		System.out.println("[CLIENT] System wird gestoppt ...");
		this.scanner.stop();

		this.cc.write("CLIENT UNREGISTER " + this.settings.clientID);
		System.exit(0);
	}

	@Override
	public void executeCommand(String command) {
		String[] cmd = command.split(" ");
		System.out.println(command);
		switch (cmd[0]) {
		case "CONNECTION_LOST":
			cc = new CloudConnector(this.settings.csIP, this.settings.csPort, this);
			break;
		case "SERVER":
			switch (cmd[1]) {
			case "CREATE":
				
				Thread thread = null;
				thread = new Thread(new Runnable(){
					
					@Override
					public void run(){
						//Erstellt neue Server
						//Command = SERVER CREATE 
						//Parameter wenn BungeecordServer = [2]: ServerType; [3]: Ram; 
						//Parameter wenn MinecraftServer = [2]: ServerType; [3]: Ram; [4]: Map; [5]: ID
						
						//Zielordner
						if(cmd.length == 4 || cmd.length == 6){
							
							File to = null;
							//Bungeecord
							if(cmd.length == 4){
							to = new File("Server\\" + cmd[2] + "");
							
							//MinecraftServer
							} else if(cmd.length == 6){
								to = new File("Server\\" + cmd[2] + "_" + cmd[5]);
								
							}
							
							if(to == null){
								return;
							}
							
							//Vorlagen
							ArrayList<File> files = new ArrayList<File>();
							switch(cmd[2]){
							case "BUNGEECORD":
								File bungeecord_basic = new File("Vorlagen\\BUNGEECORD\\");
								files.add(bungeecord_basic);
								break;
							case "LOBBY":
								File lobby_basic = new File("Vorlagen\\LOBBY\\");
								files.add(lobby_basic);
								break;
							case "LASERTAG":
								File lasertag_basic = new File("Vorlagen\\LASERTAG\\BASIC\\");
								File lasertag_lobby = new File("Vorlagen\\LASERTAG\\LOBBY\\");
								File lasertag_map = new File("Vorlagen\\LASERTAG\\MAPS\\" + cmd[4] + "\\MAP\\");
								files.add(lasertag_basic);
								files.add(lasertag_lobby);
								files.add(lasertag_map);
								
								//Extra passt nicht ins Chema
								File lasertag_mapconfi = new File("Vorlagen\\LASERTAG\\" + cmd[4] + "\\CONFI\\");
								FileUtils.copyFolder(lasertag_mapconfi, to);
								break;
							}
							
							//Dateien kopieren
							for(File file : files){
								FileUtils.copyFolder(file, to);
							}
							
							//Zus�tzliche Dateinen f�r Minecraft Server
							if(cmd.length == 6){
								
								//Serverdata f�r das Plugin
								createServerdata(cmd[2], cmd[5], to);
								
								//Server.properties configureren
								configurateProperties(to, cmd[5]);
							}
							
							//start.bat mit entsprechendem Ram erstellen
							if(cmd[2].equals("BUNGEECORD")){
								createBatch(Integer.parseInt(cmd[3]), true, to);
							} else {
								createBatch(Integer.parseInt(cmd[3]), false, to);
							}
							
							try {
								thread.sleep(1000);
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
							//Server starten
							try {
								Runtime.getRuntime().exec("cmd /c start " + to + "\\start.bat");
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
					
				});
				thread.start();
				break;
				
				/*case "CREATE":
				// TODO: Ram einbinden
				File to;
				File basic;
				switch (cmd[2]) {
				case "BUNGEECORD":
					if (cmd.length == 4) {
						// Files configurieren
						to = new File("Server\\" + cmd[2]);
						basic = new File("Vorlagen\\BUNGEECORD");

						// Ordner Kopieren
						try {
							FileUtils.copyFolder(basic, to);
						} catch (IOException e) {
							e.printStackTrace();
						}

						// Server starten
						try {
							Runtime.getRuntime().exec("cmd /c start " + to + "\\start.bat");
						} catch (IOException e) {
							e.printStackTrace();
						}
						System.out.println("[CloudClient] Es wurde ein " + cmd[2] + " Server erstellt");
					}
					break;
				case "LOBBY":
					// cmd[1] = ServerType; cmd[2] = Map; cmd[3] = Ram cmd[4] =
					// ID
					if (cmd.length == 6) {
						// Files configurieren
						to = new File("Server\\" + cmd[1] + "_" + cmd[4] + "\\");
						File all = new File("Vorlagen\\LOBBY\\");

						// Ordner Kopieren
						try {
							FileUtils.copyFolder(all, to);
						} catch (IOException e) {
							e.printStackTrace();
						}

						try {
							Properties serverdata = new Properties();
							serverdata.setProperty("servername", cmd[1] + "_" + cmd[4]);
							serverdata.setProperty("id", cmd[4]);
							FileOutputStream out = new FileOutputStream(
									new File(to.toString() + "\\plugins\\LobbySystem\\Serverdata.properties"));
							serverdata.store(out, null);
							out.close();
						} catch (IOException e) {
							e.printStackTrace();
							//Etststs
						}
						// server.properties configurieren
						configurateProperties(to, cmd[4]);

						// Server starten
						try {
							Runtime.getRuntime().exec("cmd /c start " + to + "\\start.bat");
						} catch (IOException e) {
							e.printStackTrace();
						}

						System.out.println("[CloudClient] Es wurde ein neuer Server erstellt (ID = " + cmd[4]
								+ "; Servertype = " + cmd[1] + "; Map = " + cmd[2] + ")");
					}
					break;
				case "LASERTAG":
					// cmd[1] = ServerType; cmd[2] = Map; cmd[3] = Ram cmd[4] =
					// ID
					if (cmd.length == 6) {
						String mapname = cmd[2].split("_")[1];
						System.out.println(mapname + " " + cmd[0] + " " + cmd[1] + " " + cmd[2] + " " + cmd[4]);
						// Files configurieren
						to = new File("Server\\" + cmd[1] + "_" + cmd[4] + "\\");
						basic = new File("Vorlagen\\LASERTAG\\BASIC\\");
						File lobby = new File("Vorlagen\\LASERTAG\\LOBBY\\");
						File map = new File("Vorlagen\\LASERTAG\\MAPS\\" + mapname + "\\MAP\\");
						File mapconfi = new File("Vorlagen\\LASERTAG\\MAPS\\" + mapname + "\\CONFI\\");

						// Ordner Kopieren
						try {
							FileUtils.copyFolder(basic, to);
							FileUtils.copyFolder(lobby, to);
							FileUtils.copyFolder(map, to);
							FileUtils.copyFolder(mapconfi, new File(to.toString() + "\\plugins\\Lasertag\\"));
						} catch (IOException e) {
							e.printStackTrace();
						}
						// Serverdata erstellen
						try {
							Properties serverdata = new Properties();
							serverdata.setProperty("map", mapname);
							serverdata.setProperty("servername", cmd[1] + "_" + cmd[4]);
							serverdata.setProperty("id", cmd[3]);
							FileOutputStream out = new FileOutputStream(
									new File(to.toString() + "\\plugins\\Lasertag\\Serverdata.properties"));
							serverdata.store(out, null);
							out.close();
						} catch (IOException e1) {
							e1.printStackTrace();
						}

						// server.properties configurieren
						configurateProperties(to, cmd[4]);

						// Server starten
						try {
							Runtime.getRuntime().exec("cmd /c start " + to.toString() + "\\start.bat");
						} catch (IOException e) {
							e.printStackTrace();
						}
						System.out.println("[CloudClient] Es wurde ein neuer Server erstellt (ID = " + cmd[4]
								+ "; Servertype = " + cmd[1] + "; Map = " + mapname + ")");
						// TODO: konsole auslesen
						// TODO: serververzeichniss nach gebrauch
						// stoppen/l�schen

					}
					break;
				}
				break;
				*/
				
				
				
				
			case "DELETE":
				// TODO: Wenn Server offline ist
				String name = cmd[1];
				File file = new File("Server\\");
				for (String files : file.list()) {
					if (files.equals(cmd[1])) {
						FileUtils.delFolder(new File(file.toString() + "\\" + name + "\\"));
					}
				}
				break;

			}
			break;
		}
	}

	public void configurateProperties(File file, String ID) {
		File f = new File(file.getPath() + "\\server.properties");
		try {
			Properties p = new Properties();
			FileInputStream in = new FileInputStream(f);
			p.load(in);
			in.close();
			p.setProperty("server-port", String.valueOf(Integer.parseInt(ID) + 25577));
			FileOutputStream out = new FileOutputStream(f);
			p.store(out, null);
			out.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	public static void createServerdata(String serverType, String id, File to){
		Properties serverdata = new Properties();
		serverdata.setProperty("servername", serverType + "_" + id);
		serverdata.setProperty("id", id);
		try {
			FileOutputStream out = new FileOutputStream(
					new File(to.toString() + "\\plugins\\LobbySystem\\Serverdata.properties"));
			out.close();
			serverdata.store(out, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void createBatch(int ram, boolean type, File to) {
		String jar;
		if (type == true) {
			jar = "BungeeCord";
		} else {
			jar = "craftbukkit";
		}
		File file = new File(to.getAbsolutePath(), "start.bat");
		try {
			FileWriter writer = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(writer);
			
			bw.write("@ECHO OFF\n");
			bw.write("SET BINDDIR=%~dp0\n");
			bw.write("CD /D \"%BINDIR%\"\n");
			bw.write("\"%ProgramFiles%\\Java\\jre1.8.0_101\\bin\\java.exe\" -Xincgc -Xmx" + ram + "M -jar " + jar
					+ ".jar\n");
			bw.write("PAUSE");
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class CloudConnector {
	Socket socket;
	BufferedReader br;
	PrintWriter pw;
	Thread listen;
	String command = "";
	CloudListener listener;
	

	public CloudConnector(String ip, int port, CloudListener listener) {
		
		System.out.println("[CLIENT] Verbindung zum CloudSystem wird aufgenommen (IP: " + ip + " ,Port: " + port + ")");
		this.listener = listener;
		try {
			this.socket = new Socket(ip, port);
			this.br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			this.pw = new PrintWriter(socket.getOutputStream());
			
			this.listen = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						String data;
						while ((data = br.readLine()) != null) {
							System.out.println("Test");
							listener.executeCommand(data);
						}
						close();
					} catch (IOException e) {
						close();
					}
				}
			});
			this.listen.start();
			System.out.println("[CLIENT] Verbindung erfolgreich erstellt");
			System.out.println(main.c.settings.RAM);
			write("CLIENT REGISTER " + main.c.settings.clientID + " " + main.c.settings.RAM);
		} catch (IOException e2) {
			System.out.println("[CLIENT] Verbindung fehlgeschlagen");
			System.out.println("[CLIENT] Verbindung wird in 10 Sekunden erneut aufgenommen");
			try {
				Thread.sleep(10*1000);
				main.c.cc = new CloudConnector(ip, port, listener);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void write(String s) {
		this.pw.println(s);
		this.pw.flush();
	}
	
	public void close() {
		try {
			this.socket.close();
			System.out.println("[CLIENT] Verbindung zum CloudSystem verloren");
			this.listener.executeCommand("CONNECTION_LOST");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}